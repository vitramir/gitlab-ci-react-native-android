# gitlab-ci-react-native-android

## Android SDK 26.0.2

This Docker image contains react-native and the Android SDK and most common packages necessary for building Android apps in a CI tool like GitLab CI.
